import subprocess
from json import load
import sys
import re
import os


class Git:

    DIRS = []
    TOTAL = 0
    UPDATED = 0
    OUT = None

    @staticmethod
    def load(path):
        with open(path) as j:
            Git.DIRS = load(j)

    @staticmethod
    def pull(path):
        sys.stdout.write(path.split("\\")[-1])
        subprocess.Popen(["git", "pull"], cwd=path, stdout=sys.stdout).communicate()
        Git.TOTAL += 1

    @staticmethod
    def notify():
        with open(Git.OUT, "r+") as out:
            Git.UPDATED = Git.TOTAL - len(
                re.findall("Already up to date.", out.read())
            )
        print(Git.UPDATED, "out of", Git.TOTAL, "repositories updated.")


def main():
    outputfile = sys.executable.rstrip("pythonw.exe") + ".git_scheduler_out.txt"
    path = sys.executable.rstrip("pythonw.exe") + ".git_scheduler_targets.json"
    Git.load(path)
    stdout = sys.stdout
    Git.OUT = outputfile
    with open(outputfile, "w+") as out:
        sys.stdout = out
        for target in Git.DIRS:
            Git.pull(target)
        sys.stdout = stdout
    Git.notify()


if __name__ == "__main__":
    main()

