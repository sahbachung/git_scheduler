import sys
import os
import json


def printhelp(msg=None):
    print("Git scheduler")
    print("\n\tAdd a directory to the automatic updates")
    print("\tProvide the path to the repo, or leave blank for current directory")
    exit(print(msg))


def isrepo(path):
    if not os.path.exists(path):
        return printhelp("Directory doesn't exist!")
    elif not os.path.exists(path+"\\.git"):
        return printhelp("Not a git repository!")
    return True


def main():
    print(sys.argv)
    target = sys.executable.rstrip("pythonw.exe") + ".git_scheduler_targets.json"
    if len(sys.argv) == 1:
        path = os.path.abspath(".")
    elif len(sys.argv) == 2:
        path = os.path.abspath(sys.argv[1])
    else:
        return printhelp()
    if isrepo(path):
        with open(target, "r+") as j:
            paths = json.load(j)
            paths.append(path)
        with open(target, "w+") as j:
            json.dump(paths, j)


if __name__ == "__main__":
    main()
