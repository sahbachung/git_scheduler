# -*- coding: utf-8 -*-

import pytest
from git_scheduler.skeleton import fib

__author__ = "Sebastian Streckfuss"
__copyright__ = "Sebastian Streckfuss"
__license__ = "mit"


def test_fib():
    assert fib(1) == 1
    assert fib(2) == 1
    assert fib(7) == 13
    with pytest.raises(AssertionError):
        fib(-10)
