# -*- coding: utf-8 -*-
"""
    Setup file for git_scheduler.
    Use setup.cfg to configure your project.

    This file was generated with PyScaffold 3.2.2.
    PyScaffold helps you to put up the scaffold of your new Python project.
    Learn more under: https://pyscaffold.org/
"""
import sys
import os
import json
from pkg_resources import VersionConflict, require
from setuptools import setup

try:
    require('setuptools>=38.3')
except VersionConflict:
    print("Error: version of setuptools is too old (<38.3)!")
    sys.exit(1)


if __name__ == "__main__":
    path = sys.executable.rstrip("pythonw.exe")+".git_scheduler_targets.json"
    if not os.path.exists(path):
        with open(path, "x+") as j:
            json.dump(list(), j)
    setup(use_pyscaffold=True)
